import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cscc2020fall-layout-homework';
  hide: boolean = true;
  spinner: boolean = false;

  formGroup: FormGroup;
  constructor(fb: FormBuilder, private snackBar: MatSnackBar) {
    this.formGroup = fb.group({
      name: "",
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('((?=.*[A-Z]).{8,})')]],
      gender: "",
      color: "",
    });
  }

  onSubmit(){
    console.log(this.formGroup.value)
    if(this.formGroup.get('email')?.invalid){
      this.openSnackBar('ERROR: invalid email format', 'Close', 'warning-snackbar')
    }
    else if(this.formGroup.get('password')?.invalid){
      this.openSnackBar('ERROR: invalid password format', 'Close', 'warning-snackbar')
    }else{
      this.spinner = true;
      setTimeout(() => {
        this.spinner = false;
        this.openSnackBar('Snack Time', 'nom nom nom', 'good-snackbar');
      }, 4000);
    }
  }
  openSnackBar(message: string, action?: string, sbClass?: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      panelClass: sbClass,
    });
  }
}
